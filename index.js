
// Include NPM modules
const express = require('express');
const MongoClient = require('mongodb').MongoClient;

// Create Express app
const app = express();

// Define MongoDb database information
const dbConfig = {
    url: 'mongodb://localhost:27017',
    name: 'rest-api',
    usersCollectionName: 'users'
};

// Function that connects to the MongoDB server and stores a new record in rest-api/users
const writeUserToDb = (req, res) => {

    // Connect to the MongoDB server
    MongoClient.connect(dbConfig.url,  function (err, client) {

            if (err) {
                console.log(err);
            }
            else {

                // Connected successfully to MongoDB server
                console.log('Connected successfully to server.');

                // Access the database
                const db = client.db(dbConfig.name);
                const collection = db.collection(dbConfig.usersCollectionName);

                // Get name for the new user from the HTTP GET parameter "name"
                var myUsername = req.query.name;

                // Create new user
                var myUser = {
                    name: myUsername,
                    role: 'admin'
                };

                // Store user object in the DB
                collection.insert(myUser, (err, result) => {
                    res.send( 'Object created: ' + JSON.stringify(myUser) );
                });

                // Close connection to server
                client.close();

            }
    });
};

// Function that connects to the MongoDB server and reads records from rest-api/users
const readUsersFromDb = (req, res) => {

    // Connect to the MongoDB server
    MongoClient.connect(dbConfig.url,  function (err, client) {

        if (err) {
            console.log(err);
        }
        else {

            // Connected successfully to MongoDB server
            console.log('Connected successfully to server.');

            // Access the database
            const db = client.db(dbConfig.name);
            const collection = db.collection(dbConfig.usersCollectionName);

            // Store user object in the DB
            collection.find({}).toArray((err,docs) => {
                res.send( JSON.stringify(docs) );
            });

            // Close connection to server
            client.close();

        }
    });

};

// Add route to write a new user
app.get( '/writeUser', writeUserToDb );

// Add route to read users
app.get( '/readUsers', readUsersFromDb );

// Start server on port 3000
app.listen( 3000, () => { console.log('Server ready'); } );
